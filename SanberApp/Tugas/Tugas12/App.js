import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList
} from 'react-native';

import data from './data.json';
import VideoItem from './components/videoItem';
import Icon from 'react-native-vector-icons/MaterialIcons';

const App = () => {
  return (
    <View style={styles.container}>
      <View style={styles.navBar}>
        <Image
          style={styles.image}
          source={require("./images/logo.png")}
        />
        <View style={styles.rightNav}>
          <TouchableOpacity>
            <Icon style={{ marginRight: 10 }} name="search" size={25} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Icon name="account-circle" size={25} />
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.body}>
        <FlatList
          data={data.items}
          renderItem={(video) => <VideoItem video={video.item} />}
          keyExtractor={(item) => item.id}
          ItemSeparatorComponent={() => <View style={{ height: 1, backgroundColor: '#E5E5E5' }} />}
        />
      </View>

      <View style={styles.tabBar}>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="home" size={25} />
          <Text style={styles.tabTitle}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="whatshot" size={25} />
          <Text style={styles.tabTitle}>Trending</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="subscriptions" size={25} />
          <Text style={styles.tabTitle}>Subscriptions</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="folder" size={25} />
          <Text style={styles.tabTitle}>Library</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  navBar: {
    height: 55,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  image: {
    width: 90,
    height: 22,
    resizeMode: 'contain'
  },
  rightNav: {
    flexDirection: 'row'
  },
  body: {
    flex: 1,
  },
  tabBar: {
    backgroundColor: 'white',
    height: 60,
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  tabItem: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  tabTitle: {
    fontSize: 11,
    color: '#3C3C3C'
  }
});

export default App;
