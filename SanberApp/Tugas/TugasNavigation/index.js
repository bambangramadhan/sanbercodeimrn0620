import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import LoginScreen from '../Tugas13/LoginScreen';
import RegisterScreen from '../Tugas13/RegisterScreen';
import AboutScreen from '../Tugas13/AboutScreen';
import SkillScreen from '../Tugas14/SkillScreen';
import AddScreen from './AddScreen';
import ProjectScreen from './ProjectScreen';

const Drawer = createDrawerNavigator();
const StackApp = createStackNavigator();
const Tabs = createBottomTabNavigator();

const TabScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name={'Skill'} component={SkillScreen} />
    <Tabs.Screen name={'Project'} component={ProjectScreen} />
    <Tabs.Screen name={'Add'} component={AddScreen} />
  </Tabs.Navigator>
)


const DrawerNavigator = () => (
  <Drawer.Navigator>
    <Drawer.Screen name={'About'} component={AboutScreen} />
    <Drawer.Screen name={'Main'} component={TabScreen} />
  </Drawer.Navigator>
)

export default () => (
  <NavigationContainer>
    <StackApp.Navigator>
      <StackApp.Screen name={'Login'} component={LoginScreen} options={{ headerShown: false }} />
      <StackApp.Screen name={'Register'} component={RegisterScreen} options={{ headerShown: false }} />
      <StackApp.Screen name={'Home'} component={DrawerNavigator} options={{ headerShown: false }} />
    </StackApp.Navigator>
  </NavigationContainer>
);

