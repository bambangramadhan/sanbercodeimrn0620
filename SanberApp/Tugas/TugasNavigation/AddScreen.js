import React from 'react';
import { View, Text } from 'react-native';

const AddScreen = () => {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>{'Halaman Tambah'}</Text>
    </View>
  )
};

export default AddScreen;