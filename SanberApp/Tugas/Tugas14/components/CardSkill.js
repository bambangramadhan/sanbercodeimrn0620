import React from 'react';
import { Text, StyleSheet, View } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const CardSkill = (props) => {
  return (
    <View style={styles.card}>
      <View style={styles.leftSide}>
        <Icon
          name={props.iconName}
          size={props.iconSize}
          color={props.iconColor}
        />
      </View>

      <View style={styles.mainSide}>
        <Text style={styles.skillName}>{props.skillName}</Text>
        <Text style={styles.categories}>
          {props.categories == 'Library' || props.categories == 'Framework' ?
            'Library / Framework' :
            props.categories == 'Language' ?
              'Bahasa Pemrograman' : 'Teknologi'
          }
        </Text>
        <Text style={styles.percentage}>{props.percentage}</Text>
      </View>

      <View style={styles.rightSide}>
        <Icon
          name={'chevron-right'}
          size={80}
        />
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#B4E9FF',
    width: 300,
    height: 100,
    alignSelf: 'center',
    alignItems: 'center',
    borderRadius: 5,
    elevation: 4,
    marginTop: 6,
    flexDirection: 'row'
  },
  leftSide: {
    width: 90,
    justifyContent: 'center',
    alignItems: 'center'
  },
  mainSide: {
    width: 130,
    marginLeft: 10
  },
  rightSide: {
    width: 90,
    justifyContent: 'center',
    alignItems: 'center'
  },
  skillName: {
    fontSize: 18,
    fontWeight: 'bold'
  },
  categories: {
    color: '#3EC6FF',
    fontWeight: 'bold'
  },
  percentage: {
    color: 'white',
    fontSize: 45,
    fontWeight: 'bold',
    textAlign: 'right'
  }
});

export default CardSkill;