import React from 'react';
import { Text, StyleSheet, View, FlatList, Image } from 'react-native';

import data from './skillData.json';
import CardSkill from './components/CardSkill';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const SkillScreen = () => {
  return (
    <View style={styles.container}>
      <Image
        source={require('../Tugas13/assets/sanbercodeLogo.png')}
        style={{
          resizeMode: 'contain',
          width: 150,
          height: 50,
          alignSelf: 'flex-end',
        }}
      />

      <View style={{
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 10
      }}>
        <Icon
          name={'account-circle'}
          size={25}
          color={'#3EC6FF'}
        />
        <View style={{ marginLeft: 10 }}>
          <Text style={{ fontSize: 12 }}>{'Hai,'}</Text>
          <Text>{'Bambang Ramadhan'}</Text>
        </View>
      </View>

      <View style={{
        marginLeft: 10,
        marginTop: 15,
        marginBottom: 10
      }}>
        <Text style={{ fontSize: 30 }}>
          {'SKILL'}
        </Text>
        <View style={{
          width: 300,
          height: 3,
          backgroundColor: '#3EC6FF'
        }} />
      </View>

      <View style={{
        flexDirection: 'row',
        marginLeft: 10,
        marginBottom: 8
      }}>
        <View style={[styles.categories, { width: 118 }]}>
          <Text style={{
            fontSize: 12,
            fontWeight: 'bold'
          }}>
            {'Library / Framework'}
          </Text>
        </View>
        <View style={[styles.categories, { width: 125, marginHorizontal: 4 }]}>
          <Text style={{
            fontSize: 12,
            fontWeight: 'bold'
          }}>
            {'Bahasa Pemrograman'}
          </Text>
        </View>
        <View style={[styles.categories, { width: 50 }]}>
          <Text style={{
            fontSize: 12,
            fontWeight: 'bold'
          }}>
            {'Teknologi'}
          </Text>
        </View>
      </View>

      <FlatList
        data={data.items}
        renderItem={(val) => {
          return (
            <CardSkill
              iconName={val.item.iconName}
              iconSize={70}
              skillName={val.item.skillName}
              categories={val.item.category}
              percentage={val.item.percentageProgress}
            />
          )
        }
        }
        keyExtractor={(item) => item.id}
      />

    </View >
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    marginBottom: 10
  },
  categories: {
    width: 118,
    height: 25,
    backgroundColor: '#B4E9FF',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5
  }
});

export default SkillScreen;