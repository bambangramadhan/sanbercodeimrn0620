import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

import VectorIcon from './components/VectorIcon';

const AboutScreen = () => {

  return (
    <View style={styles.container}>
      <Text style={{
        color: '#003366',
        fontSize: 28,
        fontWeight: 'bold'
      }}>
        {'Tentang Saya'}
      </Text>
      <VectorIcon
        style={{ marginVertical: 5 }}
        iconName={"user-circle-o"}
        iconSize={120}
        iconColor={'gray'}
      />
      <Text style={{
        color: '#003366',
        fontSize: 20,
        fontWeight: 'bold'
      }}>
        {'Bambang Ramadhan'}
      </Text>
      <Text style={{
        color: '#3EC6FF',
        fontSize: 14,
        fontWeight: 'bold',
        marginBottom: 10
      }}>
        {'React Native Developer'}
      </Text>

      <View style={styles.box}>
        <Text style={{
          color: '#003366',
          fontSize: 16,
          marginLeft: 10
        }}>
          {'Portofolio'}
        </Text>

        <View style={styles.separatorLine} />

        <View style={styles.wrapIconFirstBox}>
          <VectorIcon
            iconName={"gitlab"}
            iconSize={35}
            textIcon={'@bambangramadhan'}
          />
          <VectorIcon
            iconName={"github"}
            iconSize={35}
            textIcon={'@bambangramadhan'}
          />
        </View>
      </View>

      <View style={[styles.box, { height: 180 }]}>
        <Text style={{
          color: '#003366',
          fontSize: 16,
          marginLeft: 10
        }}>
          {'Hubungi Saya'}
        </Text>

        <View style={styles.separatorLine} />

        <View style={{
          marginHorizontal: 30,
          alignItems: 'center',
          marginTop: 10
        }}>
          <VectorIcon
            style={{ flexDirection: 'row' }}
            iconName={"facebook-official"}
            iconSize={35}
            textIcon={'@bmbngrmdhn'}
            styleText={{ marginLeft: 15 }}
          />
          <VectorIcon
            style={{ flexDirection: 'row', marginVertical: 10 }}
            iconName={"instagram"}
            iconSize={35}
            textIcon={'@bmbngrmdhn'}
            styleText={{ marginLeft: 15 }}
          />
          <VectorIcon
            style={{ flexDirection: 'row' }}
            iconName={"twitter"}
            iconSize={35}
            textIcon={'@bmbngrmdhn'}
            styleText={{ marginLeft: 15 }}
          />
        </View>
      </View>

      <View>

      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff'
  },
  box: {
    width: 300,
    height: 130,
    backgroundColor: '#EFEFEF',
    borderRadius: 10,
    elevation: 2,
    marginVertical: 5
  },
  separatorLine: {
    width: 280,
    height: 1,
    backgroundColor: '#003366',
    marginVertical: 3,
    alignSelf: 'center'
  },
  wrapIconFirstBox: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginHorizontal: 25,
    alignItems: 'center',
    marginTop: 20
  }
});

export default AboutScreen;