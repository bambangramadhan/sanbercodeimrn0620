import React, { useState } from 'react';
import { View, StyleSheet, Image, Text, ScrollView } from 'react-native';

import Button from './components/Button';
import TextInput from './components/TextInput';

const RegisterScreen = ({ navigation }) => {

  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [ulangiPassword, setUlangiPassword] = useState('');

  return (
    <ScrollView style={{ backgroundColor: '#ffffff' }}>
      <View style={styles.container}>
        <Image
          source={require('./assets/sanbercodeLogo.png')}
          style={{ resizeMode: 'contain', width: 300 }}
        />
        <Text style={{
          textAlign: 'center',
          color: '#003366',
          fontSize: 22
        }}>
          {'Register'}
        </Text>
        <TextInput
          value={username}
          labelText={'Username'}
          onChangeText={val => setUsername(val)}
        />
        <TextInput
          value={email}
          labelText={'Email'}
          onChangeText={val => setEmail(val)}
        />
        <TextInput
          value={password}
          labelText={'Password'}
          secureTextEntry={true}
          onChangeText={val => setPassword(val)}
        />
        <TextInput
          value={ulangiPassword}
          labelText={'Ulangi Password'}
          secureTextEntry={true}
          onChangeText={val => setUlangiPassword(val)}
        />

        <Button text={'Daftar'} style={{ marginTop: 10 }} onPress={() => navigation.push('Home')} />
        <Text style={{
          color: '#3EC6FF',
          fontSize: 20
        }}>
          {'atau'}
        </Text>
        <Button text={'Masuk ?'} style={{ backgroundColor: '#3EC6FF' }} onPress={() => navigation.push('Login')} />

      </View>
    </ScrollView>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#ffffff'
  },
});

export default RegisterScreen;