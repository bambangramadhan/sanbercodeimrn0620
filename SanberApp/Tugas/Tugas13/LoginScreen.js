import React, { useState } from 'react';
import { View, StyleSheet, Image, Text, ScrollView } from 'react-native';

import Button from './components/Button';
import TextInput from './components/TextInput';

const LoginScreen = ({ navigation }) => {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  return (
    <ScrollView style={{ backgroundColor: '#ffffff' }}>
      <View style={styles.container}>
        <Image
          source={require('./assets/sanbercodeLogo.png')}
          style={{
            resizeMode: 'contain',
            width: 300,
            marginVertical: 25
          }}
        />
        <Text style={{
          textAlign: 'center',
          color: '#003366',
          fontSize: 22,
          marginBottom: 15
        }}>
          {'Login'}
        </Text>
        <TextInput
          value={email}
          labelText={'Username / Email'}
          onChangeText={text => setEmail(text)}
        />
        <TextInput
          value={password}
          labelText={'Password'}
          secureTextEntry={true}
          onChangeText={text => setPassword(text)}
        />

        <Button
          text={'Masuk'}
          style={{ backgroundColor: '#3EC6FF', marginTop: 25 }}
          onPress={() => navigation.push('Home')}
        />
        <Text style={{
          color: '#3EC6FF',
          fontSize: 20,
          marginVertical: 10
        }}>
          {'atau'}
        </Text>
        <Button text={'Daftar ?'} onPress={() => navigation.push('Register')} />

      </View>
    </ScrollView>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#ffffff'
  },
});

export default LoginScreen;