import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

const Button = (props) => {
  return (
    <TouchableOpacity style={[styles.button, props.style]} onPress={props.onPress}>
      <Text style={[styles.textButton, props.styleText]}>
        {props.text}
      </Text>
    </TouchableOpacity>
  )
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#003366',
    width: 100,
    height: 30,
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textButton: {
    textAlign: 'center',
    fontSize: 20,
    color: 'white'
  }
});

export default Button;