import React from 'react';
import { Text, StyleSheet, View, TextInput } from 'react-native';

const TextInputDefault = (props) => {
  return (
    <View style={[styles.container, props.style]}>
      <Text style={{ color: '#003366' }}>
        {props.labelText}
      </Text>
      <TextInput
        value={props.value}
        onChangeText={props.onChangeText}
        secureTextEntry={props.secureTextEntry}
        style={[styles.textInput, props.style]}
      />
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    marginVertical: 7
  },
  textInput: {
    width: 220,
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderColor: '#003366'
  },
});

export default TextInputDefault;