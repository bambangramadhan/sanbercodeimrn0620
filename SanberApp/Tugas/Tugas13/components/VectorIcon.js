import React from 'react';
import { Text, StyleSheet, View } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

const VectorIcons = (props) => {
  return (
    <View style={[styles.container, props.style]}>
      <Icon
        name={props.iconName}
        size={props.iconSize}
        color={
          props.iconColor !== undefined ?
            props.iconColor
            :
            '#3EC6FF'}
      />
      {props.textIcon && (
        <Text style={[props.styleText, styles.textStyle]}>{props.textIcon}</Text>
      )}
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  textStyle: {
    color: '#003366',
    textAlign: 'center',
    fontWeight: 'bold'
  }
});

export default VectorIcons;