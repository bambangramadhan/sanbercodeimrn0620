// 1. SOAL CLASS

class Score {
  constructor(subject, points, email) {
    this._subject = subject;
    this._points = points;
    this._email = email;
  }

  average = () => {
    let point = this._points;
    if (typeof point === 'number') {
      console.log(`email: ${this._email}, subject: ${this._subject}, points: ${point}`);
    } else {
      let total = 0;
      for (let i = 0; i < point.length; i++) {
        total += point[i];
      }
      let avg = total / point.length;
      console.log(`email: ${this._email}, subject: ${this._subject}, points: ${avg}`);
    }
  }
}

let point1 = new Score("quiz 2", 40, 'bambangramadhan96@gmail.com');
point1.average();

let rata_rata = new Score("quiz 2", [50, 20, 30, 20], 'bambangramadhan96@gmail.com');
rata_rata.average();


// 2. SOAL Create

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

const viewScores = (data, subject) => {
  let arr = [];
  let obj = {};
  for (let i = 1; i < data.length; i++) {
    for (let j = 0; j < 1; j++) {
      obj = {
        email: data[i][0],
        subject,
        points: subject == "quiz-1" ? data[i][1] : subject == "quiz-2" ? data[i][2] : data[i][3]
      }
    }
    arr.push(obj);
  }
  console.log(arr);
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")


// 3. SOAL Recap

const recapScores = (data) => {
  let avg = 0;
  let predikat = '';
  for (let i = 1; i < data.length; i++) {
    for (let j = 0; j < data[i].length; j++) {
      avg = (data[i][1] + data[i][2] + data[i][3]) / 3;
    }
    predikat = avg > 90 ? 'honour' : avg > 80 ? 'graduate' : avg > 70 ? 'participant' : 'failed';
    console.log(`${i}. Email: ${data[i][0]}\nRata-rata: ${Math.round(avg * 10) / 10}\nPredikat: ${predikat}\n`);
  }
}

recapScores(data);