// 1. If - else

var nama = "John";
var peran = "";

if (nama == '') {
  console.log("Nama harus diisi!");
} else {
  console.log('Selamat datang di Dunia Werewolf, ' + nama);

  if (peran == '') {
    console.log('Halo ' + nama + ', pilih peranmu untuk memulai game!');
  } else if (peran == 'Penyihir') {
    console.log('Halo Penyihir ' + nama + ', kamu dapat melihat siapa yang menjadi werewolf!');
  } else if (peran == 'Guard') {
    console.log('Halo Guard ' + nama + ', kamu akan membantu melindungi temanmu dari serangan werewolf.');
  } else if (peran == 'Werewolf') {
    console.log('Halo Werewolf ' + nama + ', kamu akan memakan mangsa setiap malam!');
  } else {
    console.log('Mohon maaf ' + nama + ', peran yang kamu pilih tidak ada di daftar.');
  }
};


// 2. Switch Case

var tanggal = 1;
var bulan = 3;
var tahun = 1945;
var namaBulan;

switch (bulan) {
  case 1:
    namaBulan = 'Januari';
    break;
  case 2:
    namaBulan = 'Februari';
    break;
  case 3:
    namaBulan = 'Maret';
    break;
  case 4:
    namaBulan = 'April';
    break;
  case 5:
    namaBulan = 'Mei';
    break;
  case 6:
    namaBulan = 'Juni';
    break;
  case 7:
    namaBulan = 'Juli';
    break;
  case 8:
    namaBulan = 'Agustus';
    break;
  case 9:
    namaBulan = 'September';
    break;
  case 10:
    namaBulan = 'Oktober';
    break;
  case 11:
    namaBulan = 'Nopember';
    break;
  case 12:
    namaBulan = 'Desember';
    break;
};

if ((tanggal >= 1 && tanggal <= 31) && (bulan >= 1 && bulan <= 12) && (tahun >= 1900 && tahun <= 2200)) {
  console.log(tanggal + ' ' + namaBulan + ' ' + tahun);
} else {
  console.log('Anda salah memasukkan format!');
};