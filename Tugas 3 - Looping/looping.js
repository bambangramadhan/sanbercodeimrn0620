// No. 1 Looping While

var angka = 2;
console.log('LOOPING PERTAMA');

while (angka <= 20) {
  console.log(angka + ' - I love coding');
  angka += 2;
};

var number = 20;
console.log('LOOPING KEDUA');

while (number > 0) {
  console.log(number + ' - I will become a mobile developer');
  number -= 2;
}

// No. 2 Looping menggunakan for
console.log('==================================');

for (var i = 1; i <= 20; i++) {
  if (i % 3 == 0 && i % 2 !== 0) {
    console.log(i + ' - I Love Coding');
  } else if (i % 2 == 0) {
    console.log(i + ' - Berkualitas');
  } else if (i % 2 == 1) {
    console.log(i + ' - Santai');
  }
}

// No. 3 Membuat Persegi Panjang
console.log('==================================');

var hastag = '';
for (var i = 0; i < 4; i++) {
  for (var j = 0; j < 8; j++) {
    hastag += '#';
  };
  hastag += '\n';
};

console.log(hastag);

// No. 4 Membuat Tangga
console.log('==================================');

var pagar = '';

for (var i = 0; i < 7; i++) {
  for (let j = 0; j < 7; j++) {
    if (i >= j) {
      pagar += '#';
    };
  };
  pagar += '\n';
};

console.log(pagar);

// No. 5 Membuat Papan Catur
console.log('==================================');

var catur = '';

for (var i = 1; i <= 8; i++) {
  for (let j = 1; j <= 8; j++) {
    if ((i % 2 !== 0 && j % 2 !== 0) || (i % 2 == 0 && j % 2 == 0)) {
      catur += ' ';
    } else {
      catur += '#';
    }
  };
  catur += '\n';
};

console.log(catur);
