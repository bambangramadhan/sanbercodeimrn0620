var readBooksPromise = require('./promise.js')

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 }
]

// Lanjutkan code untuk menjalankan function readBooksPromise
var i = 0;
function readBooks(time, book) {
  readBooksPromise(time, book)
    .then(function (fulfilled) {
      i++;
      if (i < books.length) {
        if (fulfilled > books[i].timeSpent) {
          readBooks(fulfilled, books[i])
        }
      }
    })
    .catch(function (error) {
      console.log(error);
    })
};

readBooks(10000, books[0]);