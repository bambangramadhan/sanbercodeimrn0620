// A. Ascending Ten

function AscendingTen(num) {
  var ascending = [];
  if (num == undefined) {
    return -1;
  } else {
    for (let i = num; i < num + 10; i++) {
      ascending.push(i);
    };
    return ascending.join(' ');
  };
};

console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

console.log('===========================================================================');
// B. Descending Ten

function DescendingTen(num) {
  var descending = [];
  if (num == undefined) {
    return -1;
  } else {
    for (let i = num; i > num - 10; i--) {
      descending.push(i);
    };
    return descending.join(' ');
  };
}

console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1

console.log('===========================================================================');
// C. Ascending Descending

function ConditionalAscDesc(reference, check) {
  if (reference == undefined || check == undefined) {
    return -1;
  } else {
    if (check % 2 == 0) {
      return DescendingTen(reference)
    } else {
      return AscendingTen(reference)
    }
  };
}

console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1

console.log('===========================================================================');
//D. Papan Ular Tangga

function ularTangga() {
  var angka = 100;
  var data = '';

  for (var i = 0; i < 10; i++) {
    if (i % 2 == 0) {
      data += DescendingTen(angka);
      angka = data.slice(data.length - 2) - 10;
    } else {
      data += AscendingTen(angka);
      angka = data.slice(data.length - 2) - 10;
    }

    for (let j = 0; j < 19; j++) {
      if ((i % 2 == 0 && j % 2 !== 0) || (i % 2 !== 0 && j % 2 !== 0)) {
        data += ' ';
      } else {
        data;
      }
    };
    data += '\n';
  };
  return data;
}

console.log(ularTangga());

