// A. Balik String

function balikString(kata) {
  var hasil = "";
  for (var i = kata.length - 1; i >= 0; i--) {
    hasil += kata[i];
  }
  return hasil;
}

console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

console.log('===========================================================================');
// B. Palindrome

function palindrome(kata) {
  var kalimat = balikString(kata);
  if (kalimat == kata) {
    return true;
  } else {
    return false;
  }
}

console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false


console.log('===========================================================================');
// C. Bandingkan Angka

function bandingkan(num1, num2) {
  if ((num1 < 0 || num2 < 0) || (num1 == undefined && num2 == undefined) || (num1 === num2)) {
    return -1;
  } else if (num2 == undefined) {
    return num1;
  } else if (num1 > num2) {
    return num1;
  } else if (num1 < num2) {
    return num2;
  }
}

console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18