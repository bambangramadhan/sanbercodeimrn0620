// 1. Animal Class
// Release 0

class Animal {
  constructor(name, legs) {
    this._name = name;
    this._legs = legs ? legs : 4;
    this._cold_blooded = false;
  }
  get name() {
    return this._name;
  }
  get legs() {
    return this._legs;
  }
  get cold_blooded() {
    return this._cold_blooded;
  }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log('=======================');

// Release 1

// Code class Ape dan class Frog di sini
class Ape extends Animal {
  constructor(name) {
    super(name, 2);
  }
  yell() {
    console.log('Auooo');
  }
}

class Frog extends Animal {
  constructor(name) {
    super(name);
  }
  jump() {
    console.log('hop hop');
  }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 


console.log('===========================================================================');


// 2. Function to Class

class Clock {
  constructor({ template }) {
    this._template = template;
    this._timer;
  }

  render() {
    var date = new Date();
    var template = this._template;

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  stop() {
    clearInterval(timer);
  }

  start() {
    this.render();
    this._timer = setInterval(() => this.render(), 1000);
  };
};

var clock = new Clock({ template: 'h:m:s' });
clock.start();  