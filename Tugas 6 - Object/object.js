// Soal No. 1 (Array to Object)

function arrayToObject(array) {
  var obj = {};
  var now = new Date();
  var thisYear = now.getFullYear();

  if (array.length <= 0 || array == undefined) {
    console.log("");
  } else {
    for (let i = 0; i < array.length; i++) {
      for (let j = 0; j < array[i].length; j++) {
        obj = {
          firstname: array[i][0],
          lastName: array[i][1],
          gender: array[i][2],
          age: array[i][3] == undefined || array[i][3] > thisYear ? "Invalid Birth Year" : thisYear - array[i][3]
        };
      };
      console.log(`${i + 1}.`, array[i][0], array[i][1], ":", obj);
    };
  }
};

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
console.log('================================');

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
console.log('================================');

// Error case 
arrayToObject([]) // ""

console.log('===========================================================================');


// Soal No. 2 (Shopping Time)

function shoppingTime(memberId, money) {
  const sale = [['Sepatu Stacattu', 1500000], ['Baju Zoro', 500000], ['Casing Handphone', 50000], ['Sweater Uniklooh', 175000], ['Baju H&N', 250000]];
  var sort = sale.sort((a, b) => parseFloat(b[1]) - parseFloat(a[1]));
  var shopping = {};
  if (memberId == undefined || memberId == '') {
    return 'Mohon maaf, toko X hanya berlaku untuk member saja';
  } else if (money < 50000) {
    return 'Mohon maaf, uang tidak cukup;'
  } else {
    shopping = {
      memberId: memberId,
      money: money,
      listPurchased: [],
      changeMoney: money
    }

    for (let i = 0; i < sale.length; i++) {
      for (let j = 0; j < 1; j++) {
        if (shopping.changeMoney >= sort[i][1] && shopping.changeMoney >= 50000) {
          shopping.listPurchased.push(sort[i][0]);
          shopping.changeMoney -= sort[i][1];
        }
      }
    }
    return shopping;
  }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log('===========================================================================');


// Soal No. 3 (Naik Angkot)

function naikAngkot(arrPenumpang) {
  var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var hasil = [];
  var titikAwal = 0;
  var titikAkhir = 0;

  for (let i = 0; i < arrPenumpang.length; i++) {
    for (let j = 0; j < rute.length; j++) {
      if (rute[j] == arrPenumpang[i][1]) {
        titikAwal = j;
      } else if (rute[j] == arrPenumpang[i][2]) {
        titikAkhir = j;
      }
    }
    var obj = {
      penumpang: arrPenumpang[i][0],
      naikDari: arrPenumpang[i][1],
      tujuan: arrPenumpang[i][2],
      bayar: (titikAkhir - titikAwal) * 2000
    }
    hasil.push(obj);
  };
  return hasil;
};

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); //[]