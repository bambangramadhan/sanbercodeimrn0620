// Soal No. 1 (Range)

function range(startNum, finishNum) {
  var array = [];
  if (startNum == undefined || finishNum == undefined) {
    return -1;
  } else {
    if (startNum < finishNum) {
      for (var i = startNum; i <= finishNum; i++) {
        array.push(i);
      };
    } else {
      for (var j = startNum; j >= finishNum; j--) {
        array.push(j);
      };
    };
    return array;
  };
};

console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());

console.log('===========================================================================');

// Soal No. 2 (Range with Step)

function rangeWithStep(startNum, finishNum, step) {
  var data = [];
  if (startNum < finishNum) {
    for (var i = startNum; i <= finishNum; i += step) {
      data.push(i);
    };
  } else {
    for (var j = startNum; j >= finishNum; j -= step) {
      data.push(j);
    };
  };
  return data;
};

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

console.log('===========================================================================');

// Soal No. 3 (Sum of Range)

function sum(startNum, finishNum, step) {
  var jarak = step;
  var jumlah = 0;
  if (step == undefined) {
    jarak = 1;
  };

  if (startNum < finishNum) {
    for (var i = startNum; i <= finishNum; i += jarak) {
      jumlah += i;
    };
  } else if (startNum > finishNum) {
    for (var j = startNum; j >= finishNum; j -= jarak) {
      jumlah += j;
    };
  } else {
    if (startNum == undefined) {
      jumlah = 0;
    } else {
      jumlah += startNum;
    };
  };
  return jumlah;
};

console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

console.log('===========================================================================');

// Soal No. 4 (Array Multidimensi)

var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(input) {
  for (let i = 0; i < input.length; i++) {
    console.log(`Nomor ID: ${input[i][0]}`);
    console.log(`Nama Lengkap: ${input[i][1]}`);
    console.log(`TTL: ${input[i][2]}, ${input[i][3]}`);
    console.log(`Hobi: ${input[i][4]}`);
    console.log('\n');
  }
}

dataHandling(input);

console.log('===========================================================================');

// Soal No. 5 (Balik Kata)

function balikKata(kata) {
  var hasil = "";
  for (var i = kata.length - 1; i >= 0; i--) {
    hasil += kata[i];
  }
  return hasil;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));

console.log('===========================================================================');

// Soal No. 6 (Metode Array)

function dataHandling2(array) {
  array.splice(1, 1, "Roman Alamsyah Elsharawy");
  array.splice(2, 1, "Provinsi Bandar Lampung");
  array.splice(array.length - 1, 1, "Pria", "SMA Internasional Metro");

  var tanggal = array.slice(3, 4).join('/');
  var bulan = tanggal.slice(3, 5);
  var namaBulan = '';
  var sort = tanggal.split('/').sort(function (a, b) { return b - a });
  var tanggalStrip = tanggal.split('/').join('-');
  var namaAsli = array.slice(1, 2).join(' ').slice(0, 15);

  switch (bulan) {
    case '01':
      namaBulan = 'Januari';
      break;
    case '02':
      namaBulan = 'Februari';
      break;
    case '03':
      namaBulan = 'Maret';
      break;
    case '04':
      namaBulan = 'April';
      break;
    case '05':
      namaBulan = 'Mei';
      break;
    case '06':
      namaBulan = 'Juni';
      break;
    case '07':
      namaBulan = 'Juli';
      break;
    case '08':
      namaBulan = 'Agustus';
      break;
    case '09':
      namaBulan = 'September';
      break;
    case '10':
      namaBulan = 'Oktober';
      break;
    case '11':
      namaBulan = 'November';
      break;
    case '12':
      namaBulan = 'Desember';
      break;
  };

  console.log(array);
  console.log(namaBulan);
  console.log(sort);
  console.log(tanggalStrip);
  console.log(namaAsli);
}

dataHandling2(["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]);